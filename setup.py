# -*- coding: utf-8 -*-
"""
    homeserver setup
    ~~~~~~~~~~~~~~~~

    Setup script for homesever

    :copyright: (c) 2014 Jörgen Karlsson. All rights reserved.

    :license: 
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
    You should have received a copy of the GNU Affero General Public License
    along with this program (LICENCE.txt)  If not, see <http://www.gnu.org/licenses/>.
"""
from setuptools import setup, find_packages
from os import getcwd

__version__ = "unknown"
exec(open('homeservio/version.py').read())
print "homeservio: " + __version__

setup(name="homeserver",
      version=__version__,
      author= 'Jörgen Karlsson',
      author_email='jorgen@karlsson.com',
      description='Hal home server',
      long_description=open('README.txt').read(),
      packages=find_packages(),
      url = "https://bitbucket.org/jorkar",
      install_requires = [
        "hal-core>=0.7.3",
        "Flask>=0.10.1",
        "flask-admin>=1.0.8",
        "psutil",
        "autoupgrade",
        "socketIO-client==0.5.3b",
        ],
      #package_data={'': ['templates/*.*']},
      dependency_links=['https://homeservio.com/simple'],
      #data_files = [('../',['homeserver.conf'])], 
      scripts=['homeserver', 'plugin_bootstrap.py'],
      include_package_data=True,
      test_suite='homeservio.tests',
      classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Home Automation",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 2.7",
        "Unreleased :: Do not upload to PYPI"
        ]
      )
