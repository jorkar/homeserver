#!env/bin/python
# -*- coding: utf-8 -*-
"""
    homeservio
    ~~~~~~~~~~

    The homeservio homeserver

    :copyright: (c) 2014 Jörgen Karlsson

    :license: 
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
    You should have received a copy of the GNU Affero General Public License
    along with this program (LICENCE.txt)  If not, see <http://www.gnu.org/licenses/>.
"""
import os
from argparse import ArgumentParser
import logging

from autoupgrade import AutoUpgrade

from homeservio import HomeServer, __version__
from homeservio.installer import Installer
from homeservio.service import DbClient
from hal.core.network.network import EventConsumer2, EventConsumer


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as e: 
        if e.errno == EEXIST and isdir(path):
            pass
        else: 
            raise

from homeservio.loginit import loginit

logger=logging.getLogger()
#===============================================================================
# Homeserver
#===============================================================================


def cmd_run(hs, args):
    if args.plugin:
        try:
            hs.run_plugin(args.plugin)
        except ValueError as e:
            print str(e) + " Available plugins: " + ", ".join(hs.plugins)
    else:
        hs.run()

def cmd_start(hs, args):
    if args.plugin:
        print "Not yet implemented"
    else:
        hs.start()
    
def cmd_stop(hs, args):
    if args.plugin:
        print "Not yet implemented"
    else:
        hs.stop()

def cmd_restart(hs, args):
    if args.plugin:
        print "Not yet implemented"
    else:
        hs.restart()

def cmd_upgrade(hs, args):
    if args.plugin:
        if args.plugin not in hs.plugins:
            print "%s is not installed." % args.plugin
            return
        Installer.install("Homeservio-" + args.plugin, upgrade = True)
    else:
        Installer.install("homeserver", upgrade = True)
        if args.plugins:
            for plugin, version in Installer.plugins(hs.plugins):
                if version != "built-in":
                    Installer.install("Homeservio-" + plugin, upgrade = True)
            

def cmd_install(hs, args):
    if args.plugin in hs.plugins:
        print "Plugin %s already installed. Use 'homeserver upgrade' to upgrade" % (args.plugin)
        return
    package = "Homeservio-" + args.plugin
    print "trying to install " + package
    Installer.install(package)

def cmd_uninstall(hs, args):
    if args.plugin not in hs.plugins:
        print "Plugin %s is not installed." % (args.plugin)
        return
    package = "Homeservio-" + args.plugin
    Installer.uninstall(package)
        
def cmd_register(hs, args):
    email = args.email
    print "Register with live (%s)" % (email)
    hs.register(email)

def cmd_show(hs, args):
    globals()['cmd_show_' + args.show](hs, args)

def cmd_show_devices(hs, args):
    print "Devices:"
    for dev in DbClient().get_all():
        print " " + str(dev)
    

def cmd_show_plugins(hs, args):
    print hs.get_version()
    print "installed plugins:"
    for plugin, version in Installer.plugins(hs.plugins):
        print " - %s (%s)" % (plugin, version) 
    print "available plugins:"
    for plugin, version in Installer.plugins():
        print " - %s (%s)" % (plugin, version) 

def cmd_show_status(hs, args):
    print hs.get_version()
    print hs.status()
    print hs.plugins

def cmd_show_events(hs, args):
    print hs.get_version()
    print "Abort with crtl + c."
    ec = EventConsumer()
    ec.connect()
    try:
        while True:
            print str(ec.getRawEvent())
    except KeyboardInterrupt:
        ec.close()
    
    

def version():
    return HomeServer.get_version()

def main():
    parser = ArgumentParser(description = version(), add_help=False)
    parser.add_argument("-c", "--config", action="store", dest="config", default = "homeserver.conf", help="Use configuration file CONFIG")
    parser.add_argument("-v", "--version", action="version", version=version())
    parser.add_argument("-?", "-h","--help", action="help", help="show this help message and exit")
    subparsers = parser.add_subparsers(help = "type homeserver <cmd> --help for help on subcommands", dest ='command')

    run_parser = subparsers.add_parser("run", help = "run homeserver/plugin in foreground")
    run_parser.add_argument("plugin", nargs='?', help = "name of plugin")

    start_parser = subparsers.add_parser("start", help = "start homeserver/plugin in background")
    start_parser.add_argument("plugin", nargs='?', help = "name of plugin")
    
    stop_parser = subparsers.add_parser("stop", help = "stop homeserver/plugin running in background")
    stop_parser.add_argument("plugin", nargs='?', help = "name of plugin")

    restart_parser = subparsers.add_parser("restart", help = "restart homeserver/plugin running in background")
    restart_parser.add_argument("plugin", nargs='?', help = "name of plugin")

    upgrade_parser = subparsers.add_parser("upgrade", help = "upgrade upgrade homeserver/plugin to latest version")
    upgrade_parser.add_argument("plugin", nargs='?', help = "name of plugin")
    upgrade_parser.add_argument("--plugins",  action="store_true", help = "upgrade all plugins")

    install_parser = subparsers.add_parser("install", help = "install plugin")
    install_parser.add_argument("plugin", help = "name of plugin")
    
    uninstall_parser = subparsers.add_parser("uninstall", help = "uninstall plugin")
    uninstall_parser.add_argument("plugin", help = "name of plugin")

    register_parser = subparsers.add_parser("register", help = "register this homeserver with live")
    register_parser.add_argument("email", help = "the email adress you have registered with homeservio live")


    show_parser = subparsers.add_parser("show", help = "show command, type 'homeserver show --help' for more info")
    showsubparsers = show_parser.add_subparsers(help = "type homeserver <cmd> --help for help on subcommands", dest ='show')
    showsubparsers.add_parser("devices", help = "show devices")
    showsubparsers.add_parser("plugins", help = "show status of plugins")
    showsubparsers.add_parser("status", help = "show status")
    showsubparsers.add_parser("events", help = "show events")
    
    args=parser.parse_args()
    hs = HomeServer(args.config)
    globals()['cmd_' + args.command](hs, args)

if __name__ == "__main__":
    main()
    