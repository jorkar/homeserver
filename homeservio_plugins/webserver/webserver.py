#!env/bin/python

import logging
import time
from flask import Flask
from .admin_views import init

app = Flask(__name__)
init(app)

from homeservio.plugin import Plugin

import views


class Webserver(Plugin):
    
    def run(self, name, settings = {}):
        debug = "debug" in settings.keys()
        host = settings.setdefault("host", "0.0.0.0")
        port = int(settings.setdefault("port",8080))

        app.run(host = host, port = port, debug = debug, threaded = True)
        # todo this shall be plugin loop
        while True:
            time.sleep(1)
              
if __name__ == "__main__":
    Webserver().run()