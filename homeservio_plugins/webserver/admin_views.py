from flask.ext.admin import Admin
from flask.ext.admin.model import BaseModelView
from homeservio.service import DbClient # TODO move as plugin call


def init(app):
    admin = Admin(app)
    db = DbClient()
    admin.add_view(DeviceView(db))

def get_data(view, context, model, name):
    rv=[]
    if model.meta is not None:
        for k in model.meta:
            rv.append("%s=%s" %(str(k), str(getattr(model, k))))
    return ", ".join(rv)

def get_action(view, context, model, name):
    rv=[]
    if model.meta is not None:
        for k in model.meta:
            type=model.meta.get(k).get('type', None)
            ro = model.meta.get(k).get('readonly', False)
            try:
                if type.get('enum', None):
                    html = ""
                    for e in type.get('enum', None):
                        if e == getattr(model, k):
                            html += '<button class="btn btn-default active">%s</button>' % (e)
                        else:
                            html += '<button class="btn btn-default">%s</button>' % (e)
                    rv.append(html)
                else:
                    rv.append("%s=%s" %(str(k), str(getattr(model, k))))
            except AttributeError: 
                rv.append("%s=%s" %(str(k), str(getattr(model, k))))
    return ", ".join(rv)

class DeviceView(BaseModelView):
    can_create = False
    can_edit = False
    can_delete = False
    column_formatters = dict(data=get_data, action=get_action)
    list_template = 'device.html'

    
    def __init__(self, db):
        self.db = db
        db.__name__ = "Devices"
        super(DeviceView, self).__init__(db)
    
    def get_pk_value(self, model):
        return model.name
    
    def scaffold_list_columns(self):
        return ["name", "last_change", "data"]
    
    def scaffold_sortable_columns(self):
        return None
    
    def scaffold_form(self):
        return None
    
    def get_list(self, page, sort_field, sort_desc, search, filters):
        l = self.db.getAll()
        return len(l), l

        