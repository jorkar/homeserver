#!env/bin/python
# -*- coding: utf-8 -*-
"""
    Supervisor - a supervisor plugin for homeservio homeserver
    
    :copyright: (c) 2014 Jörgen Karlsson

    :license: 
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
    You should have received a copy of the GNU Affero General Public License
    along with this program (LICENCE.txt)  If not, see <http://www.gnu.org/licenses/>.
"""
import time
from datetime import datetime
import psutil
import logging

from homeservio.plugin import Plugin
from homeservio.plugin.api import send_device_update_event, DeviceFactory, MetaFactory

log = logging.getLogger()

class Supervisor(Plugin):
    INTERVAL = 600
        
    def run(self, name, settings):
        sensor = DeviceFactory.create("homeserver")
        sensor.add_meta("cpu", MetaFactory.gauge("cpu", "%"))
        sensor.add_meta("memory", MetaFactory.gauge("memory", "%"))
        try:
            interval = int(settings.get("interval", Supervisor.INTERVAL))
        except Exception as e:
            log.error("Error in configuration %s", str(e))
            interval = Supervisor.INTERVAL
        while 1:
            sensor.last_change = datetime.utcnow()
            sensor.cpu = psutil.cpu_percent()
            sensor.memory = psutil.virtual_memory().percent
            send_device_update_event("supervisor." + sensor.name, sensor.toJson())
            log.debug("Cpu: %s Memory: %s", sensor.cpu, sensor.memory)
            time.sleep(interval)
            
