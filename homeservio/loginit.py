import logging
from logging.handlers import SMTPHandler, RotatingFileHandler
from os.path import abspath
from config import Config

import os

def loginit(config, name = __name__ ):
    c = Config.instance(config)
    logfile = c.get("HOMESERVER", "logfile", abspath("/var/log/homeserver.log"))
    mail = c.exists("HOMESERVER", "mail")
    debug = c.exists("HOMESERVER", "debug")
    if mail:
        username = c.get("HOMESERVER", "mail-username", None)
        password = c.get("HOMESERVER", "mail-password", None)
        port = c.get("HOMESERVER", "smtp-port", None)
        server = c.get("HOMESERVER", "smtp-server", None)
        admins = c.get("HOMESERVER", "mail-to", None)
    logger = logging.getLogger()
    if mail:
        credentials = None
        credentials = (username, password)
        mail_handler = SMTPHandler((server, port), 'no-reply@' + server, admins, name + ' failure', credentials)
        mail_handler.setLevel(logging.ERROR)
        logger.addHandler(mail_handler)
    file_handler = RotatingFileHandler(logfile, 'a', 1 * 1024 * 1024, 10)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(logging.Formatter(' %(asctime)s ' + name + ' %(levelname)s: %(message)s [%(threadName)s %(filename)s:%(funcName)s:%(lineno)d]'))
    logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(logging.Formatter(' %(asctime)s ' + name + ' %(levelname)s: %(message)s [%(threadName)s %(filename)s:%(funcName)s:%(lineno)d]'))
    logger.addHandler(console_handler)
    
    if debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        