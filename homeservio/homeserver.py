# -*- coding: utf-8 -*-
"""
    homeservio
    ~~~~~~~~~~

    The homeservio homeserver

    :copyright: (c) 2014 Jörgen Karlsson

    :license: 
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
    You should have received a copy of the GNU Affero General Public License
    along with this program (LICENCE.txt)  If not, see <http://www.gnu.org/licenses/>.
"""
import time 
import sys
import subprocess
import os
from os.path import abspath, dirname, realpath, basename, splitext, join, isdir, expanduser, exists
from argparse import ArgumentParser
from errno import EEXIST
import logging
from random import randint

import zmq

from hal.core.util import Daemon
from hal.core.network import EventProxy, Broker 
from hal.core.encryption import RSAcrypto
import hal.core.version


from .loginit import loginit
from homeservio.adapter import LiveAdapter
from homeservio import Config, __version__, PluginManager, HsPlugin, plugin
from homeservio.service import Db
from .installer import Installer

logger = logging.getLogger()

class HomeServer(Daemon):
    
    def __init__(self, config = None):
        self._config = Config.instance(config)
        loginit(config, __name__)
        self._pidfile = self._config.get("HOMESERVER", "pidfile", "/var/run/homeserver.pid")
        self._stderr = self._config.get("HOMESERVER", "stderr", "/dev/null")
        self._stdout = self._config.get("HOMESERVER", "stdout", "/dev/null")
        super(HomeServer, self).__init__( self._pidfile, stdout = self._stdout, stderr = self._stderr)
        self.web = None
        self.running = False
        self.ep = None
        plugin_dirs = (abspath(f) for f in (join(p, "homeservio_plugins") for p in sys.path + [abspath("")]) if exists(f))
        self.pm = PluginManager(plugin_dirs, self._config)
        self.plugins = list(self.pm.plugins.keys())
        self.liveAdapter = None
        self.guid = self._config.get("LIVE", "guid") 
        self.live_key = None
        self.my_private_key_file = self._config.get("HOMESERVER", "private_key", abspath("homeserver_id_rsa.key")) 
        self.my_public_key_file = self._config.get("HOMESERVER", "public_key", abspath("homeserver_id_rsa.pub")) 
        self.server = self._config.get("LIVE", "server", "https://homeservio.com") 

    @staticmethod    
    def get_version():
        return "Homeserver version %(ver)s (hal-core %(hal-core)s, pyzmq %(pyzmq)s, zmq %(zmq)s)" % { "ver": __version__ , 
                                                                                   "hal-core": hal.core.version.__version__,
                                                                                   "pyzmq": zmq.__version__,
                                                                                   "zmq": zmq.zmq_version()}
    def get_version_full_json(self):
        rv = {'homeserver': __version__,
              'hal-core': hal.core.version.__version__,
              'pyzmq': zmq.__version__,
              'zmq': zmq.zmq_version()}
        for plugin, version in Installer.plugins(self.plugins):
            rv[plugin] = version
        return rv
    
    def run_plugin(self, plugin):
        """ run plugin in foreground """
        self.pm.run_plugin(plugin)
          
    def run(self):
        logger.info(self.get_version())
        
        # TODO we can generate an uuid here instead, and save it then we fix a small util that can find it for any 
        # homeserver on the local network (by shotgun approach connectng since we do not know the ip)
        
        # todo : live should care for it's own config
        self.running = True
        #os.chdir(BASEDIR)
        try:
            self.ep = EventProxy().start()
            self.broker = Broker().start()
            if self.guid and self.my_private_key_file and self.my_public_key_file:
                self.liveAdapter = LiveAdapter(self.server, self.guid, self.my_private_key_file, self.my_public_key_file,
                                               self._config, versions = self.get_version_full_json()).start()
            self.db = Db().start()
            # temp solution oif issue #7 a real fix should be done, either:
            # ping to db before start of plugins or
            # same in plugin_bootstrap instead
            time.sleep(1)
            # plugins
            self.pm.start_plugins()

            while self.running:
                for name, plugin in self.pm.plugins.iteritems():
                    if not (plugin.disabled or plugin.is_running()):
                        logger.warning("Plugin %s is not running." % (name))
                time.sleep(60)

        except KeyboardInterrupt:
            logger.info("Keyboardinterrupt, exiting")
            self.close()
        except Exception:
            logger.exception("Unhandled exeption")
            raise
        finally:
            self.close()
        
    def close(self):
        self.running = False
        logger.warning("Stopping")
        self.pm.stop_plugins()
        if self.liveAdapter:
            self.liveAdapter.stop()
            self.liveAdapter = None
        if self.db:
            self.db.stop_and_wait()
        if self.broker:
            self.broker.stop_and_wait()
        if self.ep:
            self.ep.stop_and_wait()
            self.ep = None
        zmq.Context.instance().term()
        
    def stopping(self):
        self.close()
        sys.exit(1)
        
    def status(self):
        try:
            with file(self.pidfile,'r') as pf:
                message = "running\n pid: %s" % pf.read().strip() 
        except IOError:
            message = "not running"
        return message
    
    def write_config(self):
        self._config.write()
        
    def register(self, email):
        # first generate guid
        if self._config.get("HOMESERVER", "registered", False):
            print "Already registered"
            return
        if self.guid:
            print "Guid already exists, reusing %s" % (self.guid)
        else:
            self.guid = str(hex(randint(0x0B00000000000000,0x0BFFFFFFFFFFFFFF))).rstrip('L')
        self._config.set("LIVE", "guid", self.guid)
        print "Guid: %s" % (self._config.get("LIVE", "guid"))
        # generate keypair
        pubfile = self.my_public_key_file
        keyfile = self.my_private_key_file
        if not (exists(pubfile) and exists(keyfile)):
            priv, pub = RSAcrypto.generate_keypair()
            with open(pubfile, 'w') as f:
                f.write(pub)
                print "Created and public key in %s" % (pubfile)
            with open(keyfile, 'w') as f:
                f.write(priv)
                print "Created and private key in %s" % (keyfile)
            os.chmod(keyfile, 0600)
        self._config.set("HOMESERVER", "private_key", keyfile)
        self._config.set("HOMESERVER", "public_key", pubfile)
        self._config.set("HOMESERVER", "registered", False)
        self._config.set("HOMESERVER", "email", email)
        #LiveAdapter(self.server, self.guid, self.my_private_key_file, self.my_public_key_file).register(email)
        print "A new house will be created, associated with the guid %s and the contents of your public key file %s" % (self.guid, self.my_public_key_file)
        print "\nYou have to restart (start) homeserver!"
        print "=======================================\n"
        
        self.write_config()
        