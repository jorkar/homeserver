import ConfigParser
import codecs
from os import path
from os import curdir, environ
from ConfigParser import NoOptionError, NoSectionError

class Config(object):
    _instance = None
    
    
    @staticmethod
    def instance(filename=None):
        if Config._instance == None:
            Config._instance = Config(filename)
        return Config._instance

    def __init__(self, filename = None):
        self.parser = ConfigParser.RawConfigParser(allow_no_value=True)
        if filename:
            self._filename = path.abspath(filename)
            try:
                with open(self._filename) as f:
                    self.parser.readfp(f)
            except IOError:
                pass
                
    def get_filename(self):
        return self._filename
        
    def get(self, section, option, default=None):
        try:
            return self.parser.get(section, option)
        except (NoOptionError, NoSectionError):
            self.set(section, option, default)
            return default
        
    def set(self, section, option, value):
        try:
            self.parser.set(section, option, value)
        except NoSectionError:
            self.parser.add_section(section)
            self.parser.set(section, option, value)

    def getsection(self, section):
        try:
            return dict(self.parser.items(section))
        except (NoSectionError):
            return {}

    def getboolean(self, section, option, default=False):
        try:
            return self.parser.getboolean(section, option)
        except (NoOptionError, NoSectionError):
            self.set(section, option, default)
            return default

    def exists(self, section, option):
        try:
            self.parser.get(section, option)
            return True
        except (NoOptionError, NoSectionError):
            return False
        
    def write(self):
        with open(self._filename, 'wb') as configfile:
            self.parser.write(configfile)
