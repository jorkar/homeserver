import logging
import time

from hal.core.network import Message
import hal.core.model  

from ._api import Api

def register_device_change_request(function):
    """
    Register for device change request.
    This event will happen for every device that shall change in the system.
    You have to check the adress of the device to be sure it's yours.
    
    Function must have the signature: function(address, device)
    The address is the address assigned to the device to be changed
    device is a json with changes 
    
    TODO add name of plugin always so we only get our own ones!
    """
    Api.instance().getEc().register(Message.DCR, function)

    
def send_device_update_event(address, msg):
    """
    Send a device update event upstream. Shall be sent whenever a device has changed.
    
    The DB in the core of Homeservio homeserver will see to that only changes are propagated to live.
    
    Parameters:
        address: address of the device. Shall start with <plugin_name>.
        msg: the serialized device. (Json serialized)
    """
    Api.instance().getEp().sendDeviceUpdateEvent(address,  msg)
    

def event_loop():
    """
    Make sure to run this in your run method
    """
    Api.instance().start()
    while 1:
        time.sleep(10)
    



class MetaFactory(object):
    """
    Factory class to make meta primitives
    """

    @staticmethod
    def gauge(name = "value", unit = None):
        """
        Create gauge primitive meta. A gauge is readonly and can be formatted. Can be presented in the gui as text or 
        a gauge/meter or similar.
        """
        return { 'type': "number", 'readonly': True, 'format': "{0} " + unicode(unit) if unit is not None else "{0}"}
    
class DeviceFactory(hal.core.model.DeviceFactory):
    """
    Factory class to make devices.
    
    It is good practice to use these factory methods in conjuction with MetaFactory above. This will make the plugin 
    future safe when demand for features in the gui increases.
    
    The meta data in the devices is used for the gui, and should be read as from the user view-point. E.g. "readonly" denotes 
    that the value is readonly from the end-user perspective, thus he/she cannot change it. Your plugin **can** change it.
    
    ## supported meta
    
    type: number | integer | enum : [ list of valid enums ]
    readonly: True | False (default: False)
    format: format_string. put {0} in place of the value. 
    max: maximum of value (number or integer, inclusive)
    min: minimum of value (number or integer, inclusive)
    
    All values are internally stored as strings, giving maximum flexibility for change.
    
    ## Some rules of the game
    * Encoding shall be done in utf-8.
    * All times shall be in UTC.
    
    """

    @staticmethod
    def create(name = None, meta = None):
        """
        Create device freely.
        Parameters:
            name: name of device
            meta: meta as either dict or created through MetaFactory
        """
        return hal.core.model.Device(name = name, meta = meta)

    @staticmethod
    def gauge(name = None, value=None, unit = None, value_name = "value"):
        """
        Creates a gauge.
        Parameters:
            name: name of gauge
            value: initial value
            unit: unit of gauge. Used by GUI for formatting gauge.
            value_name: name of value of gauge. Default 'value'.
        """
        dev = hal.core.model.Device(name = name)
        dev.add_meta(value_name, MetaFactory.gauge(name = value_name, unit = unit))
        setattr(dev, value_name, value)
        return dev

    @staticmethod
    def multi_gauge(name = None, values = [("value", None, None)]):
        """ 
        create a gauge with several values
        parameters:
            name: name of multiGauge
            values: list of 3-tuples with name, unit, value, e.g. [("temp", "F", 23.4), ("humidity", "% RF", 65)] 
        """
        dev = hal.core.model.Device(name = name)
        for name, unit, value in values:
            dev.add_meta(name, MetaFactory.gauge(name = name, unit = unit))
            setattr(dev, name, value)
        return dev

    @staticmethod
    def switch(name = None, state = None,):
        """
        Creates a switch on/off style.
        Parameters:
            name: name of gauge
            state: initial state ('on' | 'off' )
        """
        meta = { 'state': { 'type': { "enum": ['off', 'on'] } } } 
        return hal.core.model.Device(name = name , state = state, meta = meta)
    
    @staticmethod
    def dimmer(name = None, state = None, level = None):
        """
        Creates a dimmer.
        A dimmer will typically be shown as a slider in the GUI.
        Parameters:
            name: name of gauge
            state: initial state ('on' | 'off' | 'dim' )
            level: initial level (0 <= level <= 100)
        """
        meta = { 
                'state': { 'type': { "enum": ['off', 'on', 'dim' ] } } ,
                'level': { 'type':  "integer", 'max' : 100, 'min' : 0 } 
                } 
        return hal.core.model.Device(name=name , state = state, level=level, meta = meta)

    @staticmethod
    def inverter(name = None, state = None, fan = None, mode = None, temp = None):
        """
        See this as an example of a complex device. This is a bit experimental still, and is a preparation
        for the inverter plugin (halird plugin).
        """
        meta = {
                'fan':  { 'type': { "enum" : ['auto', 'lo', 'med', 'hi', 'hipower', 'eco'] } },
                'mode': { 'type': { 'enum':  ['auto','cool', 'dry', 'fan', 'heat'] } },
                'temp': { 'type': "integer", 'max': 30, 'min': 18 },
                'state': { 'type': { "enum": ['off', 'on'] }}
                } 
        return hal.core.model.Device(name = name, state = state, fan = fan, mode = mode, temp = temp, meta = meta)