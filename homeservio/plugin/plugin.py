import abc
from ._api import Api



class Plugin(object):
    """ common base object to Plugins. Plugins shall inherit this object """

    def __init__(self, name, settings):
        """ If you override this, you have to call super method.
            Use init instead."""
        self.name = name
        self.settings = settings
        self.init(name, settings)
    
    def init(self, name, settings):
        """ Override this method to make initializations """
        pass
    

    @abc.abstractmethod
    def run(self, name, settings = None):
        """ Override this method.
        Settings: a dict of settings from the config file """
        raise NotImplementedError
        return
    
    def stop(self):
        """ Override this method if you want to clean up when stopping"""
        
    def _stop(self):
        """
        run after stop. Do not override this method, used for internal shutdown
        """
        Api.destroy()
        

