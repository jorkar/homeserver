
from hal.core.network import EventProducer, EventConsumer2


class Api(object):
    """
    Api singleton for internal use
    """
    _instance = None
    
    @classmethod
    def instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance
    
    @classmethod
    def destroy(cls):
        if cls._instance is None:
            return
        else:
            cls._instance.exit()
        cls._instance = None
    
    def __init__(self):
        self._ep = None
        self._ec = None

    def getEp(self):
        if self._ep is None:
            self._ep = EventProducer()
        return self._ep
    
    def getEc(self):
        if self._ec is None:
            self._ec = EventConsumer2("EventConsumer")
        return self._ec
    
    def start(self):
        if self._ec is not None:
            self._ec.start()

    def exit(self):
        if self._ep is not None:
            self._ep.close()
        if self._ec is not None:
            self._ec.stop()