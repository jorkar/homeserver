from hal.core.network import EventProducer, EventConsumer
from homeservio import Config


class Adapter(object):
    
    def __init__(self):
        self._ep = EventProducer()
        self._ec = EventConsumer()
        
    def alias(self, name):
        return Config.alias(name)
    