import threading
from struct import pack
import time

import zmq
import logging
import json
from socketIO_client import SocketIO, BaseNamespace, exceptions
from base64 import b64encode
from requests.packages.urllib3.exceptions import ProtocolError

from hal.core.network import EventConsumer, EventProducer, Message, Port, Address
from hal.core.encryption import EncryptionEngine, RSAcrypto
from hal.core.util import EverThread
from zmq.error import ContextTerminated


class LiveNamespace(BaseNamespace):
    PATH = "/live/homeserver"
    log = logging.getLogger()
    
    def __init__(self, *args, **kwargs):
        self.ep = EventProducer()
        self.live = None
        super(LiveNamespace, self).__init__(*args, **kwargs)
        
    def close(self):
        self.ep.close()
    
    def on_connect(self):
        self.log.info("Connected to Live")

    def on_disconnect(self):
        self.log.info("Disconnected from Live")

    def on_live_error(self, *args):
        message = args[0]
        self.log.error("Error from live: %s", message.get('message', 'Unknown error'))
        if message.get('forbidden', False):
            self.log.error("Forbidden to log in to live")
            if self.live:
                self.live.close()
            
    def on_logged_in(self):
        self.log.info("Logged in to live")
        
    def on_please_login(self):
        self.log.info("Please login")
        self.live.login_cb()

    def on_registered(self):
        self.log.info("Registered")
        self.live.registered_cb()

    def on_dur(self):
        self.log.info("DUR from Live")
        self.ep.send_multipart([Message.DUR])

    def on_dcr(self, *args):
        self.log.info("DCR from Live %s", args[0])
        message = json.loads(args[0])
        address = message.get("address", "")
        device = message.get("device", "")
        if address and device:
            self.ep.sendDeviceChangeRequest(address.encode("utf-8"), 
                json.dumps(device, separators=(',',':'), ensure_ascii=False).encode('utf8'))
    
class LiveAdapter(EverThread):
    ''' 
        Live adapater
        Message to live shall be from a dealer port with identity = the guid got from live.
        The first message after connection should be a "hello"
        
        
        The message shall be multipart: [ message_type , device ] where device can be repeated and shall be encoded according to hal_pb2
        
        Received messages are exactly similar
        
        Security will be added through encryption and key exchange using zmq 4 when that is stable
         
    '''
    NAME = "live"
    PROTOCOL_VERSION = "1"
    RESERVED = ""
    
    def __init__(self, server, guid, private_key_file = None, public_key_file = None, config = None, versions={}):
        self._context = zmq.Context.instance()
        self.counter = {"SENT" :0, "RECEIVED" :0}
        self._server = server
        self.guid = guid 
        self._private_key_file = private_key_file
        self._public_key_file = public_key_file
        self.live = None
        self.socketIO = None
        self._config = config
        self.ec = None
        self.poll = None
        self.log = logging.getLogger()
        self._versions = versions
        if self._config:         
            self.registered = self._config.get("HOMESERVER", "registered", False) == "True"
            self.email = self._config.get("HOMESERVER", "email", "")
        super(LiveAdapter, self).__init__("LiveAdapter")
    
    def registered_cb(self):
        self.registered = True
        self._config.set("HOMESERVER", "registered", True)
        self._config.write()
        
    def _connect_live(self):
        connected = False
        while not connected and self._running:
            logging.getLogger().info("Connecting to %s", self._server)
            try:
                self.socketIO = SocketIO(self._server)
                connected = True
            except ProtocolError:
                time.sleep(10)
        if connected:
            self.live = self.socketIO.define(LiveNamespace, LiveNamespace.PATH)
            self.live.live = self
            
    def login_cb(self):
        with open(self._private_key_file, "r") as f:
            key = RSAcrypto.import_key(f.read())
        crypto = RSAcrypto(key, None)
        if self.registered:
            token = b64encode(crypto.sign(str(self.guid)))
            self.live.emit('login', {'guid': self.guid, 'token': token, 'versions': self._versions});
        else:
            with open(self._public_key_file, "r") as f:
                public_key = f.read()
            token = b64encode(crypto.sign(str(self.guid) + public_key + self.email))
            self.live.emit('register',  {'guid': self.guid, "public_key": public_key, 'email': self.email,
                                         'token': token, 'versions': self._versions })
    
    def _disconnect_live(self):
        if self.live:
            self.live.close()
        if self.socketIO and self.socketIO.connected:
            self.socketIO.disconnect()
        self.live = None
        self.socketIO = None
    
    def init(self):
        self.ec = EventConsumer()
        self.ec.register(Message.DCE)
        self.ec.connect()
        self.poll = zmq.Poller()
        self.poll.register(self.ec.socket, zmq.POLLIN)
        self.log.info("Liveadapter starting")
    
    def run_forever(self):
        try:
            if self.live is None:
                self._connect_live()
            sockets = dict(self.poll.poll(10))
            if sockets.get(self.ec.socket) == zmq.POLLIN:
                msg = self.ec.socket.recv_multipart()
                self.counter["SENT"] += 1
                self.log.debug("Send message to Live, %s", str(msg))
                address = msg[0][len(Message.DCE):]
                self.live.emit('dce', {"address": address, "device": msg[1]})
            self.socketIO.wait(seconds=0.010)
        except (exceptions.ConnectionError, ProtocolError):
            self._disconnect_live()
        except ContextTerminated:
            self.stop()

    def clean_up(self):
        try:
            self._disconnect_live()
            if self.ec is not None:
                self.ec.close()
        except ContextTerminated:
            pass
        finally:
            self.ec = None
