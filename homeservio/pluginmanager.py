from os import listdir, path
from os.path import abspath, join, dirname, isdir, exists
from subprocess import Popen, call
import logging
import time

import psutil
import sys

log = logging.getLogger()

class HsPlugin(object):
    RUNNER = next((abspath(f) for f in (join(p, "plugin_bootstrap.py") for p in sys.path) if exists(f)), '')
    
    #RUNNER = abspath(join(dirname(__file__), "plugin_bootstrap.py"))

    def __init__(self, name, directory, filename):
        self.name = name
        self.file = abspath(join(directory, filename))
        self.disabled = False
        self.process = None

    def start(self, config = None, config_filename = None, foreground = False):
        if self.process:
            log.debug("Process already started for plugin %s" % self.name)
        else:
            args = [self.RUNNER , self.file, str(config), config_filename]
            log.info("Starting plugin %s %s" % (self.name, str(args)))
            if foreground:
                call(args)
            else:
                self.process = Popen(args)
            
    def is_running(self):
        return self.process is not None and self.process.poll() is None  

    def stop(self):
        self.kill_child_processes()
        retry = 5
        while self.is_running() and retry > 0:
            log.debug("Stopping plugin %s (pid:%s)" % (self.name, str(self.process.pid)))
            self.process.terminate()
            if self.is_running():
                time.sleep(0.5)
            retry -= 1
        retry = 5
        while self.is_running() and retry > 0:
            log.debug("Killing plugin %s (pid:%s)" % (self.name, str(self.process.pid)))
            self.process.kill()
            if self.is_running():
                time.sleep(0.5)
            retry -= 1
        self.process = None
        
    def kill_child_processes(self):
        if not self.process:
            return
        pid = self.process.pid
        try:
            p = psutil.Process(pid)
        except psutil.NoSuchProcess:
            return
        for cpid in p.get_children(recursive=True):
            logging.getLogger().debug("Terminating child %s" % cpid.pid)
            cpid.terminate()
            retry = 5
            while cpid.is_running() and retry > 0:
                time.sleep(0.1)
                retry -= 1
            if cpid.is_running():
                logging.getLogger().debug("Killing child %s" % cpid.pid)
                cpid.kill()
        
class PluginManager(object):

    def __init__(self, directories, config):
        self.plugin_directories = directories
        self.plugins = {}
        self.config = config
        for d in self.plugin_directories:
            for f in listdir(d):
                name, ext = path.splitext(path.basename(f))
                #c = name.title()
                if name == "__init__" or name.endswith("_version"):
                    continue
                if ext == ".py" or isdir(abspath(join(d,f))):
                    if not self.plugins.get(name):
                        self.plugins[name] = HsPlugin(name, d, name + ext)
                    
    def run_plugin(self, name, foreground = True):
        plugin = self.plugins.get(name, None)
        c_section = "plugin:%s" % name 
        if plugin:
            plugin.start(self.config.getsection(c_section), self.config.get_filename(), foreground = foreground)
        else:
            raise ValueError("Plugin not found")
        
    def start_plugins(self): 
        for name, plugin in self.plugins.iteritems():
            c_section = "plugin:%s" % name 
            if self.config.exists(c_section, "disable"):
                log.info("Plugin %s disabled by configuration", name)
                plugin.disabled = True
            else:
                try:
                    self.run_plugin(name, foreground = False)
                except Exception:
                    log.exception("Failed starting plugin %s" % name)
                    plugin.disabled = True


    def stop_plugins(self): 
        for unused_name, plugin in self.plugins.iteritems():
            plugin.stop()
            

    def check_plugins_running(self):
        rv = True
        for unused_name, plugin in self.plugins.iteritems():
            rv = (plugin.disabled or plugin.is_running()) and rv
        return rv
    
