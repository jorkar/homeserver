import urllib
from bs4 import BeautifulSoup
import re
import pkg_resources
from os import execl, environ
from sys import executable, argv
import pip
import re
import shlex

class PkgNotFoundError(Exception):
    """No package found"""

class NoVersionsError(Exception):
    """No versions found for package"""
    
def normalize_version(v):
    """Helper function to normalize version
    Returns a comparable object
    Args:
        v (str) version, e.g. "0.1.0"
    
    """
    rv = []
    for x in v.split("."):
        try:
            rv.append(int(x))
        except ValueError:
            for y in re.split("([0-9]+)", x):
                try:
                    if y != '':
                        rv.append(int(y))
                except ValueError:
                    rv.append(y) 
    return rv

class Installer(object):
    _instance = None
        
    @classmethod
    def instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance
    
    def __init__(self):
        self.proxy = environ.get('http_proxy', None)
        self.index = "https://homeservio.com/simple"

    @classmethod
    def install(cls, package_name, upgrade = False, dependencies = True):
        x = ""
        if upgrade:
            x += " --upgrade" 
        if not dependencies:
            x += " --no-deps"
        installer = cls.instance()
        cmd = "install %s %s -i %s" % (x, package_name, installer.index)
        return installer.pip(cmd)

    @classmethod
    def uninstall(cls, package_name):
        return cls.instance().pip('uninstall %s -y' % (package_name))

    @classmethod
    def plugins(cls, installed_plugins = None):
        rv=[]
        if installed_plugins:
            for plugin in installed_plugins:
                try:
                    version = pkg_resources.get_distribution("homeservio-%s" % (plugin)).version
                except pkg_resources.DistributionNotFound:
                    version = "built-in"
                rv.append((plugin, version) )
        else:
            rv.extend(cls.instance().get_plugins())
        return rv 

    def pip(self, cmd):
        pip_args = shlex.split(cmd)
        if self.proxy:
            pip_args[:0] = ['--proxy', self.proxy]
        print "Executing 'pip %s'" % (" ".join(pip_args))
        return pip.main(pip_args)
        
    def get_plugins(self):
        rv=[]
        try:
            html = urllib.urlopen(self.index)
        except IOError:
            print "Could not connect to %s" % self.index
            return [-1]
        soup = BeautifulSoup(html.read())
        for link in soup.find_all('a'):
            text = link.get_text()
            if text.startswith("homeservio_"):
                name = text[len("homeservio_"):]
                rv.append((name, self.get_plugin_version(name)))
        return rv
                
    def get_plugin_version(self, plugin, url = None):
        pkg = "homeservio_" + plugin
        if url is None:
            url = "{}/{}/".format(self.index, pkg)
        try:
            html = urllib.urlopen(url)
        except IOError:
            print "Could not connect to %s" % url
            return "unknown"
        if html.getcode() != 200:
            return "unknown"
        soup = BeautifulSoup(html.read())
        versions = []
        for link in soup.find_all('a'):
            text = link.get_text()
            try:
                version = re.search( pkg + '-(.*)\.tar\.gz', text).group(1)
                versions.append((normalize_version(version), version))
            except AttributeError:
                pass
        if len(versions) == 0:
            return "unknown"
        return max(versions)[1]
        

        
        
    def upgrade_if_needed(self, restart = True, dependencies = False):
        """ Upgrade the package if there is a later version available.
            Args:
                restart, restart app if True
                dependencies, update dependencies if True (see pip --no-deps)
        """
        if self.check():
            print "Upgrading %s" % self.pkg
            self.upgrade(dependencies)
            if restart:
                self.restart()
            
    def upgrade(self, dependencies = False):
        """ Upgrade the package unconditionaly
            Args:
                dependencies: update dependencies if True (see pip --no-deps)
            Returns True if pip was sucessful
        """
        pip_args = []
        proxy = environ.get('http_proxy')
        if proxy:
            pip_args.append('--proxy')
            pip_args.append(proxy)
        pip_args.append('install')
        pip_args.append(self.pkg)
        if self.index is not None:
            pip_args.append('-i')
            pip_args.append("{}/simple/".format(self.index))
        if not dependencies:
            pip_args.append("--no-deps")
        if self._get_current() != [-1]:
            pip_args.append("--upgrade")
        a=pip.main(initial_args = pip_args)
        return a==0
        
    def restart(self):
        """ Restart application with same args as it was started.
            Does **not** return
        """
        print "Restarting " + executable + " " + str(argv) 
        execl(executable, *([executable]+argv))
        
    def check(self):
        """ Check if pkg has a later version
            Returns true if later version exists.
        """
        current = self._get_current()
        highest = self._get_highest_version()
        return highest > current 
    
    def _get_current(self):
        try:
            current = normalize_version(pkg_resources.get_distribution(self.pkg).version)
        except pkg_resources.DistributionNotFound:
            current = [-1]
        return current
    
    def _get_highest_version(self):
        url = "{}/{}/".format(self.index, self.pkg).replace('//', '/')
        try:
            html = urllib.urlopen(url)
        except IOError:
            print "Could not connect to %s" % url
            return [-1]
        if html.getcode() != 200:
            raise PkgNotFoundError
        soup = BeautifulSoup(html.read())
        versions = []
        for link in soup.find_all('a'):
            text = link.get_text()
            try:
                version = re.search( self.pkg + '-(.*)\.tar\.gz', text).group(1)
                versions.append(normalize_version(version))
            except AttributeError:
                pass
        if len(versions) == 0:
            raise NoVersionsError()
        return max(versions)
