# -*- coding: utf-8 -*-
import unittest
from homeservio.plugin.api import DeviceFactory
from datetime import datetime

        
class TestInverter(unittest.TestCase):

    def testOne(self):
        inverter = DeviceFactory.inverter("hej")
        self.assertEqual(inverter.state, None)
        inverter.state = 'on'
        self.assertEqual(inverter.state, 'on')
        #
        #inverter.state = 0
        #self.assertEqual(inverter.state, 'off')
        #with self.assertRaises(AttributeError):
        #    inverter.state = 'konstigt'
        self.assertIsNone(inverter.mode)
        #inverter.mode = 'AUTO'
        inverter.mode = 'auto'
        self.assertEqual(inverter.mode, 'auto')
        #inverter.mode = 4
        #self.assertEqual(inverter.mode, 'fan')
        #with self.assertRaises(AttributeError):
        #    inverter.mode=7
        #with self.assertRaises(AttributeError):
        #    inverter.mode='weird'
        #with self.assertRaises(AttributeError):
        #    inverter.fan = 17
        self.assertIsNone(inverter.fan)
        inverter.fan ='eco'
        self.assertEqual(inverter.fan, 'eco')
        #inverter.fan = 3
        #self.assertEqual(inverter.fan, 'med')
            
    def testSerialize(self):
        inverter = DeviceFactory.inverter("one", mode='auto', fan='hi', temp=19 )
        self.assertEqual(inverter.temp, 19)
        self.assertEqual(inverter.fan, 'hi')
        serialized = inverter.SerializeToString()
        inverter2 = DeviceFactory.createFromString(serialized)
        self.assertEqual(inverter, inverter2)
        self.assertEqual(str(inverter), str(inverter2))
        inverter2.temp = 18
        self.assertNotEqual(inverter, inverter2)
        self.assertNotEqual(str(inverter), str(inverter2))
        
        
class TestGauge(unittest.TestCase):
    
    def testGaugeComp(self):
        sensor1 = DeviceFactory.gauge("one", 11.3, u"°C")
        sensor2 = DeviceFactory.gauge("one", 11.4, u"°C")
        self.assertNotEqual(sensor1, sensor2)
        sensor2.value = 11.3
        self.assertEqual(sensor1,sensor2)

    def testGaugeSerialize(self):
        sensor1 = DeviceFactory.gauge("one", 11.3, u"°C")
        s1_serialized = sensor1.toJson(True)
        sensor2 = DeviceFactory.createFromString(s1_serialized)
        self.assertEqual(sensor1, sensor2)
        self.assertEqual(str(sensor1), str(sensor2))

class TestMultiGauge(unittest.TestCase):
    
    def testGaugeComp(self):
        sensor1 = DeviceFactory.multi_gauge("one", [("temp", u"°C", 11.3), ("humidity", u"% RH", 75)])
        sensor2 = DeviceFactory.multi_gauge("one", [("temp", u"°C", 11.4), ("humidity", u"% RH", 75)])
        self.assertNotEqual(sensor1, sensor2)
        self.assertEqual(sensor1.temp, 11.3)
        self.assertEqual(sensor1.humidity, 75)
        self.assertEqual(sensor2.temp, 11.4)
        sensor2.temp = 11.3
        self.assertEqual(sensor2.temp, 11.3)
        self.assertEqual(sensor1,sensor2)

    def testGaugeSerialize(self):
        sensor1 = DeviceFactory.multi_gauge("one", [("temp", u"°C", 11.3), ("humidity", u"% RH", 75)])
        s1_serialized = sensor1.toJson(True)
        sensor2 = DeviceFactory.createFromString(s1_serialized)
        self.assertEqual(sensor1, sensor2)
        self.assertEqual(str(sensor1), str(sensor2))
        s1_serialized = sensor1.toJson(False)
        sensor2 = DeviceFactory.createFromString(s1_serialized)
        self.assertEqual(sensor2.temp, 11.3)
        self.assertEqual(sensor2.humidity, 75)

class TestSwitch(unittest.TestCase):
    
    def testSwitchComp(self):
        sw1 = DeviceFactory.switch("sw1", "on")
        sw2 = DeviceFactory.switch("sw2", "on")
        self.assertTrue(sw1 == sw2)
        
    def testSwitchSerialize(self):
        sw1 = DeviceFactory.switch("sw1", "on")
        self.assertEqual("on", sw1.state)
        sw1_serialized = sw1.SerializeToString()
        sw2 = DeviceFactory.createFromString(sw1_serialized)
        self.assertEqual(sw1, sw2)
        self.assertEqual(str(sw1), str(sw2))
        sw2.state = "off"
        self.assertNotEqual(sw1, sw2)
        self.assertNotEqual(str(sw1), str(sw2))
        
class TestDimmer(unittest.TestCase):
    
    def testDimmerComp(self):
        sw1 = DeviceFactory.dimmer("sw1", "dim", 23)
        sw2 = DeviceFactory.dimmer("sw2", "dim", 23)
        self.assertTrue(sw1 == sw2)
        sw2.level = 1
        self.assertTrue(sw1 != sw2)
        sw1.level = 1
        self.assertTrue(sw1 == sw2)
        
    def testDimmerSerialize(self):
        sw1 = DeviceFactory.dimmer("sw1", "on")
        self.assertEqual("on", sw1.state)
        sw1_serialized = sw1.SerializeToString()
        sw2 = DeviceFactory.createFromString(sw1_serialized)
        self.assertEqual(sw1, sw2)
        self.assertEqual(str(sw1), str(sw2))
        sw2.state = "off"
        self.assertNotEqual(sw1, sw2)
        self.assertNotEqual(str(sw1), str(sw2))

    def testDimmerNegative(self):
        dim1 = DeviceFactory.dimmer("dimmer", "on")
        self.assertEqual("on", dim1.state)
        self.assertEqual(None, dim1.level)
        dim1.level = 255
        self.assertEqual(255, dim1.level)
        dim1.level = 0
        self.assertEqual(0, dim1.level)
        #with self.assertRaises(AttributeError):
        #    dim1.level = 256
        #with self.assertRaises(AttributeError):
        #    dim1.level = -1
