import unittest
import logging
import time

import zmq

from hal.core import network
from homeservio.plugin.api import DeviceFactory
from .. import service
logging.basicConfig()

class TestDb(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls._broker = network.Broker().start()
        cls._proxy = network.EventProxy().start()
        cls._db = service.Db().start()
        cls._ep = network.EventProducer()
        cls._ep.connect("test")

    @classmethod
    def tearDownClass(cls):
        cls._ep.close()
        cls._db.stop_and_wait()
        cls._broker.stop_and_wait()
        cls._proxy.stop_and_wait()

    def testGet(self):
        c = service.DbClient()
        start = len(c.getAll())
        rsp = c.get("nonexistent")
        self.assertIsNone(rsp)
        sensor = DeviceFactory.gauge("test1", 20.0, "x")
        self._ep.sendDeviceUpdateEvent(sensor.name , sensor.toJson())
        time.sleep(1) # to have the db be updated since this is asynchrounuos
        s1 = c.get("test1")
        self.assertEqual(sensor, s1)
        s2 = c.get("test2")
        self.assertIsNone(s2)
        
        sensor2 = DeviceFactory.gauge("test1", 20.1, "y")
        self._ep.sendDeviceUpdateEvent(sensor.name , sensor2.toJson())
        time.sleep(1) # to have the db be updated since this is asynchrounuos
        s1 = c.get("test1")
        self.assertTrue(sensor2 == s1)
        
        sensor3 = DeviceFactory.gauge("test2", 20.3, "z")
        self._ep.sendDeviceUpdateEvent(sensor3.name , sensor3.toJson())
        time.sleep(1) # to have the db be updated since this is asynchrounuos

        l = c.getAll()
        self.assertEqual(2 + start, len(l))
        self.assertTrue(sensor2 in l)
        self.assertTrue(sensor3 in l)
            
    def testDCE(self):
        c = service.DbClient()
        ec = EC()
        
        nof, msg = ec.getNofMsgAndLast()
        rsp = c.get("TESTDCE")
        self.assertIsNone(rsp)
        
        sensor = DeviceFactory.gauge("TESTDCE", 20.0, "C")
        
        self._ep.sendDeviceUpdateEvent(sensor.name , sensor.toJson())
        nof, msg = ec.getNofMsgAndLast()
        self.assertEqual(nof, 1)

        sensor.value = 20.1
        self._ep.sendDeviceUpdateEvent(sensor.name , sensor.toJson())
        nof, msg = ec.getNofMsgAndLast()
        self.assertEqual(nof, 1)
        
        self._ep.sendDeviceUpdateEvent(sensor.name , sensor.toJson())
        nof, msg = ec.getNofMsgAndLast()
        self.assertEqual(nof, 0)
        
        sensor.value = 20.2
        self._ep.sendDeviceUpdateEvent(sensor.name , sensor.toJson())
        sensor.value = 20.1
        self._ep.sendDeviceUpdateEvent(sensor.name , sensor.toJson())
        nof, msg = ec.getNofMsgAndLast()
        self.assertEqual(nof, 2)

        sensor.value = 20.1
        self._ep.sendDeviceUpdateEvent(sensor.name , sensor.toJson())
        sensor.value = 20.1
        self._ep.sendDeviceUpdateEvent(sensor.name , sensor.toJson())
        sensor.value = 20.1
        self._ep.sendDeviceUpdateEvent(sensor.name , sensor.toJson())
        sensor.value = 20.2
        self._ep.sendDeviceUpdateEvent(sensor.name , sensor.toJson())
        nof, msg = ec.getNofMsgAndLast()
        self.assertEqual(nof, 1)
        
class EC(object):
    ''' test mock object event consumer'''
    
    def __init__(self):
        self.ec = network.EventConsumer()
        self.ec.register(network.Message.DCE)
        self.ec.connect()
        self.poll = zmq.Poller()
        self.poll.register(self.ec.socket, zmq.POLLIN)

    def getNofMsgAndLast(self):
        nof = 0
        msg = None
        cont = True
        while cont:
            sockets = dict(self.poll.poll(1000))
            if sockets.get(self.ec.socket) == zmq.POLLIN:
                msg = self.ec.socket.recv_multipart()
                nof +=1
            else:
                cont = False
        return nof, msg
    
        