
         
#===============================================================================
# hal_database
#===============================================================================
import logging
import threading
from datetime import datetime

from hal.core.model import DeviceFactory
from hal.core.network import Service, NetworkException, EventConsumer2, EventProducer, Message, Err, Client
import types
from hal.core.util import EverThread

logger = logging.getLogger()

class Db(Service):
    NAME = "db"
    GETALL = "getall"
    GET = "get"
    
    def __init__(self):
        self._db = {}
        self._servers = {}
        self._ec = EventConsumer2(Db.NAME)
        self._ep = EventProducer()
        super(Db, self).__init__(Db.NAME)
    
    def init(self):
        super(Db, self).init()
        self._ec.register(Message.DUE, self.eventDUE)
        self._ec.register(Message.DUR, self.eventDUR)
        self._ec.start()
        self._ep.connect(self.NAME)
    
    def clean_up(self):
        super(Db, self).clean_up()
        self._ec.stop()
        self._ep.close()
    
    def eventDUR(self, unused, unused2):
        for k,v in self._db.iteritems():
            self._ep.sendDeviceChangedEvent(k, v.toJson(meta = True))
    
    def eventDUE(self, address, msg):
        if address in self._db:
            diff = self._db.get(address).updateFromString(msg[1])
            if diff is not None:
                logger.debug("Sending DCE %s %s", address, unicode(diff))
                self._ep.sendDeviceChangedEvent(address, diff.toJson())
        else:
            device = DeviceFactory.createFromString(msg[1])
            logger.debug("New device in DB %s %s", address, str(device))
            logger.debug("Sending DCE %s %s", address, str(device))
            self._ep.sendDeviceChangedEvent(address, device.toJson(meta = True))
            self._db[address] = device 
        
    def request(self, msg):
        ''' request from client '''
        cmd, dummy, name = msg.partition(" ")
        if cmd == Db.GETALL:
            list = []
            for k,v in self._db.iteritems():
                list.append(v.SerializeToString())
            return list
        elif cmd == Db.GET:
            device = self._db.get(name)
            if device:
                return device.SerializeToString()
            else:
                return ""
        else:
            emsg = "{} unknown command {}".format(Err.INTERNAL_SERVER_ERROR, msg[0])
            logger.debug("DB: %s", emsg)
            return msg
        
class DbClient(Client):
    
    def __init__(self, address = None):
        Client.__init__(self, address = address)
    
    def get(self, name):
        try:
            reply = self.request(Db.NAME, Db.GET + " " + name)
        except NetworkException as e:
            logger.warning("Network error during get, %s", str(e))
            return None
        if len(reply) == 0:
            return None
        else:
            return DeviceFactory.createFromString(reply)

#    @deprecated
    def getAll(self):
        """ deprecated, use get_all """
        return self.get_all()
    
    def get_all(self):
        try:
            reply = self.request(Db.NAME, Db.GETALL)
        except NetworkException as e:
            logger.warning("Network error during getAll, %s", str(e))
            return None
        if isinstance(reply, types.StringTypes):
            return [DeviceFactory.createFromString(reply)]
        else:
            l = []
            for d in reply:
                l.append(DeviceFactory.createFromString(d))
            return l 
        
