#!env/bin/python

from sys import argv
import sys
from os import path
import logging
import signal
from importlib import import_module
from ast import literal_eval

import zmq

from homeservio.loginit import loginit
from homeservio.plugin.api import Api

class StreamToLogger(object):
    def __init__(self, heading = "", log_level=logging.INFO):
        self.log_level = log_level
        self.heading = heading.upper()
        
    def write(self, buf):
        for line in buf.rstrip().splitlines():
            logging.getLogger().log(self.log_level, "%s: %s" % (self.heading, line.rstrip()))

class Bootstrap(object):
    
    def __init__(self, plugin_cls, name, settings):
        for sig in [signal.SIGTERM, signal.SIGINT, signal.SIGHUP, signal.SIGQUIT]:
            signal.signal(sig, self._handler)

        self._name = name
        self._settings = settings
        self._plugin = plugin_cls(name = self._name, settings = self._settings)
        
    def run(self):
        logging.getLogger().info("[plugin:%s] starting" % (self._name))
        self._plugin.run(name = self._name, settings = self._settings)
    
    def stop(self):
        logging.getLogger().info("[plugin:%s] stopping" % (self._name))
        self._plugin.stop()
        self._plugin._stop()
        
    def _handler(self, signum = None, frame = None):
        """ Handler for signals """
        logging.getLogger().warning("[plugin:%s] Signal %s caught, stopping" % (self._name, str(signum)))
        self.stop()
        _exit()

    
def plugin_bootstrap(args):
    """ run plugin with right sys path, not depending on where it sits"""
    filename = args[0]
    settings = literal_eval(args[1]) if len(args) > 1 else {}
    config_filename = args[2] if len(args) > 2 else None


    plugin_dir = path.abspath(path.dirname(filename))
    plugin_module, unused_plugin_ext = path.splitext(path.basename(filename))
    plugin_class = plugin_module.title()

    loginit(config_filename, plugin_module)
    log = logging.getLogger()
    loglevel = settings.get("loglevel", "info").lower()
    if loglevel == "debug":
        log.setLevel(logging.DEBUG)
    elif loglevel == "info":
        log.setLevel(logging.INFO)
    elif loglevel == "warning":
        log.setLevel(logging.WARNING)
    elif loglevel == "error":
        log.setLevel(logging.ERROR)
    sys.stdout = StreamToLogger("STDOUT", logging.INFO)
    sys.stderr = StreamToLogger("STDERR", logging.WARNING)
    if plugin_dir not in sys.path:
        sys.path.append(plugin_dir)
    import_module(plugin_module)
    plugin_cls = getattr(sys.modules[plugin_module], plugin_class)

    try:
        plugin = Bootstrap(plugin_cls, name = plugin_module, settings = settings)
        plugin.run()
    except (SystemExit, KeyboardInterrupt):
        log.debug("[plugin:%s] exiting", plugin_module)
        plugin.stop()
        _exit()
    except:
        log.exception("[plugin:%s] unexpected exception", plugin_module)
        raise
        _exit(1)

def _exit(status = 0):
    zmq.Context.instance().term()
    Api.destroy()
    sys.exit(status)
    

if __name__ == "__main__":
    if len(argv) > 1:
        plugin_bootstrap(argv[1:])
    else:
        print "Exactly one argument shall be given (filename)"
        sys.exit(1)