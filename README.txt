Homeservio homeserver
=====================

The homeserver is the base of the Homeservio home automation system. For
more information see https://homeservio.com

::

    Copyright (C) 2014 Jörgen Karlsson. All rights reserved.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Supported platforms
-------------------

Currently tested on Linux/debian with Python 2.7. Python 3 is not
supported.

Installation and configuration
------------------------------

See

-  https://homeservio.com/docs/installation
-  https://homeservio.com/docs/configuration

Release notes
-------------

0.10.6
~~~~~~

-  Added built-in plugin supervisor
-  Fix issue #5 where files where not included in the dist

0.10.5
~~~~~~

-  Removal of decisionmaker

0.10.4
~~~~~~

-  Easier registration of homeserver
-  Live adapter more robust towards network errors

0.10.3
~~~~~~

-  Fix of live adapter

0.10.2
~~~~~~

-  Faster response time when changing device
-  Move pypiserver to https
-  Name issue fixed

0.10.1
~~~~~~

-  SSL timeout error fixed

0.10.0
~~~~~~

-  support of new interface to live (socketio)
-  new command "homeserver show events"

0.9.10
~~~~~~

-  cli enhancements
-  basic web interface

0.9.9
~~~~~

-  fixed fault in startup of plugin

0.9.8
~~~~~

-  fix dependency

0.9.7
~~~~~

-  Adding cli commands:

   run run homeserver/plugin in foreground start start homeserver/plugin
   in background stop stop homeserver/plugin running in background
   restart restart homeserver/plugin running in background upgrade
   upgrade upgrade homeserver/plugin to latest version install install
   plugin uninstall uninstall plugin status show status plugins show
   status of plugins

0.9.6
~~~~~

-  support for multi\_gauge

0.9.4
~~~~~

-  api enhancements

0.9.3
~~~~~

-  halird removed
-  tellstick removed
-  api updated with DeviceFactory
-  plugin directories extended

0.9.2
~~~~~

-  Simulator break out as plugin

0.9.1
~~~~~

-  Bugfixes, new release of hal-core
-  Adapter corrections

0.9.0
~~~~~

-  Adaptations for new data format as of hal-core 0.6
-  Even more robust communication to live

0.8.9
~~~~~

-  Support unit for sensors

0.8.6, 0.8.7
~~~~~~~~~~~~

-  Plugin simulator more controlled values

0.8.5
~~~~~

-  Plugin simulator now supports setting switches
-  Plugin simulator now reports real cpu usage and memory usage once per
   minute
-  Plugin tellstick vital fault corrected
-  Fixed installation problems

0.8.4
~~~~~

-  Fix installation issues
-  Installed plugins now working

0.8.3
~~~~~

-  Logging for plugins
-  Stop of plugins (even for subprocess)

0.8.2
~~~~~

-  Basic plugin system

Info
====

-  Homepage: https://homeservio.com

